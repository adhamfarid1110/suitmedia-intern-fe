import Head from "next/head";
import Image from "next/image";
import ImageCarousel from "./components/ImageCarousel";
import ValueCard from "./components/ValueCard";
import { Alert, Button, Snackbar, TextField } from "@mui/material";
import { useForm } from "react-hook-form";
import { useState } from "react";

export default function Home() {
    // snackbar
    // Feedbacks for users
    const [open, setOpen] = useState(false);
    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }
        setOpen(false);
    };

    // React Hook Form
    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
        reset,
    } = useForm();

    const onSubmit = (data) => {
        console.log(data);
        data;
        reset({
            name: "",
            email: "",
            message: "",
        });
        handleClick();
    };

    return (
        <>
            <Head>
                <title>Suitmedia | Adham</title>
                <meta
                    name="description"
                    content="A simple app for Suitmedia Intern"
                />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <ImageCarousel />
            <section className="value-section padded">
                <h1>OUR VALUES</h1>
                <div className="values-card-section">
                    <ValueCard title="INNOVATIVE" />
                    <ValueCard title="LOYALTY" />
                    <ValueCard title="RESPECT" />
                </div>
            </section>
            <form onSubmit={handleSubmit(onSubmit)}>
                <section className="contact-us-section padded">
                    <h1>CONTACT US</h1>

                    <div className="field-text">
                        <label htmlFor="name">Name</label>
                        <TextField
                            id="name"
                            variant="outlined"
                            {...register("name", {
                                required: "This field is required.",
                            })}
                            error={Boolean(errors.name)}
                            helperText={errors.name?.message}
                        />
                    </div>
                    <div className="field-text">
                        <label htmlFor="email">Email</label>
                        {/* prettier-ignore */}
                        <TextField
                            id="email"
                            variant="outlined"
                            {...register("email", {
                              required: 'Email is required',
                                pattern: {
                                  value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
                                  message: "Invalid email address.",
                                },
                            })}
                            error={Boolean(errors.email)}
                            helperText={errors.email?.message}
                        />
                    </div>
                    <div className="field-text">
                        <label htmlFor="message">Message</label>
                        <TextField
                            id="message"
                            variant="outlined"
                            multiline
                            rows={4}
                            {...register("message", {
                                required: "This field is required.",
                            })}
                            error={Boolean(errors.message)}
                            helperText={errors.message?.message}
                        />
                    </div>
                    <Button variant="contained" type="submit">
                        Submit
                    </Button>
                </section>
            </form>
            {/* alert */}
            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert
                    onClose={handleClose}
                    severity="success"
                    sx={{ width: "100%" }}
                >
                    Email has been sent! This only a Gimmick :)
                </Alert>
            </Snackbar>
            ;
        </>
    );
}
