import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";

import { ImFacebook2, ImTwitter } from "react-icons/im";

import "/styles/globals.scss";
import "/styles/home.scss";

import "/styles/components/navbar.scss";
import "/styles/components/value-card.scss";

import Hamburger from "hamburger-react";
import { useWindowSize } from "/hooks/useWindowSize";

export default function App({ Component, pageProps }) {
    const [navActive, setNavActive] = useState(false);
    const [navSticky, setNavSticky] = useState(false);

    const { width } = useWindowSize();
    const isMobile = width < 768;

    const [isOpen, setOpen] = useState(false);

    return (
        <>
            <nav className={`nav`}>
                <section className="center">
                    <div className="left">
                        <div className="logo">
                            <h1>Company</h1>
                        </div>
                    </div>
                    {/* if navbar untuk mobile */}
                    {isMobile ? (
                        <>
                            {isOpen ? (
                                <>
                                    <div className="menu-bar">
                                        <Hamburger
                                            toggled={isOpen}
                                            toggle={setOpen}
                                        />
                                    </div>
                                    <div className={`menu-list mobile`}>
                                        <div class="dropdown">
                                            <div className="menu-item">
                                                <Link href="/">About</Link>
                                            </div>
                                            <div class="dropdown-content">
                                                <a href="#1">History</a>
                                                <a href="#2">Vision Mission</a>
                                            </div>
                                        </div>
                                        <div className="menu-item">
                                            <Link href="/">Our Work</Link>
                                        </div>
                                        <div className="menu-item">
                                            <Link href="/works/">Our Team</Link>
                                        </div>
                                        <div className="menu-item">
                                            <Link href="/contact/">
                                                Contact
                                            </Link>
                                        </div>
                                    </div>
                                </>
                            ) : (
                                <div className="menu-bar">
                                    <Hamburger
                                        toggled={isOpen}
                                        toggle={setOpen}
                                    />
                                </div>
                            )}
                        </>
                    ) : (
                        // {/* if navbar untuk desktop*/}
                        <div
                            className={`menu-list ${navActive ? "active" : ""}`}
                        >
                            <div class="dropdown">
                                <div className="menu-item">
                                    <Link href="/">About</Link>
                                </div>
                                <div class="dropdown-content">
                                    <a href="#1">History</a>
                                    <a href="#2">Vision Mission</a>
                                </div>
                            </div>
                            <div className="menu-item">
                                <Link href="/">Our Work</Link>
                            </div>
                            <div className="menu-item">
                                <Link href="/works/">Our Team</Link>
                            </div>
                            <div className="menu-item">
                                <Link href="/contact/">Contact</Link>
                            </div>
                        </div>
                    )}
                </section>
            </nav>
            <Component {...pageProps} />
            <footer>
                <small>Copyright &#169; 2016. PT Company</small>
                <div className="socmed-icons">
                    <Link href={"https://facebook.com"}>
                        <ImFacebook2 />
                    </Link>
                    <Link href={"https://twitter.com"}>
                        <ImTwitter />
                    </Link>
                </div>
            </footer>
        </>
    );
}
