import Image from "next/image";
import { FaRegLightbulb, FaBalanceScale } from "react-icons/fa";
import { RiBankFill } from "react-icons/ri";

export default function ValueCard({ title }) {
    let backgroundColor = "";
    let borderColor = "";

    if (title == "INNOVATIVE") {
        backgroundColor = "#DD8174";
        borderColor = "#D35B4A";
    } else if (title == "LOYALTY") {
        backgroundColor = "#739773";
        borderColor = "#5D795B";
    } else if (title == "RESPECT") {
        backgroundColor = "#6295BF";
        borderColor = "#4D80A9";
    }

    return (
        <div
            className="value-card"
            style={{
                backgroundColor: backgroundColor,
                borderColor: borderColor,
            }}
        >
            <div className="logo-value">
                {
                    /* prettier-ignore*/
                    {
                        "INNOVATIVE": <FaRegLightbulb />,
                        "LOYALTY": <RiBankFill />,
                        "RESPECT": <FaBalanceScale />,
                    }[title]
                }
            </div>

            <h2>{title}</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer
                quis nisi a tellus consequat venenatis.
            </p>
            {title != "RESPECT" ? (
                <div
                    className="segitiga-unyu-kanan"
                    style={{
                        backgroundColor: borderColor,
                    }}
                ></div>
            ) : (
                <></>
            )}
        </div>
    );
}
