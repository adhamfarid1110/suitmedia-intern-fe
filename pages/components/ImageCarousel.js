import { Slide } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";

import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from "react-icons/md";

const spanStyle = {
    padding: "20px",
    background: "#efefef",
    color: "#000000",
};

const divStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundSize: "cover",
    height: "100vh",
};
const slideImages = [
    {
        url: "https://cdn.discordapp.com/attachments/1000437373240361102/1128718550132080640/bg.jpg",
        caption:
            "THIS IS A PLACE WHERE TECHNOLOGY & CRETIVITY FUSED INTO DIGITAL CHEMISTRY.",
    },
    {
        url: "https://cdn.discordapp.com/attachments/1000437373240361102/1128718549897191524/about-bg.jpg",
        caption: `WE DON'T HAVE THE BEST BUT WE HAVE THE GREATEST TEAM.`,
    },
];

export default function ImageCarousel() {
    const indicators = (index) => <div className="indicator-carousel"></div>;

    const properties = {
        prevArrow: (
            <button className="slider-btn">
                <MdKeyboardArrowLeft />
            </button>
        ),
        nextArrow: (
            <button className="slider-btn">
                <MdKeyboardArrowRight />
            </button>
        ),
    };

    return (
        <div className="slide-container">
            <Slide {...properties} indicators={indicators} autoplay={false}>
                {slideImages.map((slideImage, index) => (
                    <div key={index}>
                        <div
                            style={{
                                ...divStyle,
                                backgroundImage: `url(${slideImage.url})`,
                            }}
                        >
                            <section className="section-sliders-caption">
                                <div className="caption-content">
                                    <h1>{slideImage.caption}</h1>
                                </div>
                            </section>
                        </div>
                    </div>
                ))}
            </Slide>
        </div>
    );
}
