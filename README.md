# Suitmedia-Intern-FE

## Appliers Data
- Adham Muhammad Farid
- adham.muhammad@ui.ac.id
- Sistem Informasi, Fakultas Ilmu Komputer, Universitas Indonesia

## Description
- A project for completing internship test task.

## Deployment Link
- [Deployment Link thru Vercel](https://suitmedia-intern-fe-pv44-pzwc856gg-adhamfarid11.vercel.app)

## Screenshots
![Hero Section](https://cdn.discordapp.com/attachments/1000437373240361102/1128762872034504734/image.png)
![Our Values Section](https://cdn.discordapp.com/attachments/1000437373240361102/1128762933950816306/image.png)
![Contact](https://cdn.discordapp.com/attachments/1000437373240361102/1128762973532455104/image.png)

### Email Validation with Regex
![Email Validation with Regex](https://cdn.discordapp.com/attachments/1000437373240361102/1128763076083191960/image.png)